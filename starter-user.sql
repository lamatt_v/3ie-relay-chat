--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.6
-- Dumped by pg_dump version 9.3.2
-- Started on 2015-08-04 09:24:40

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 178 (class 3079 OID 11756)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 1989 (class 0 OID 0)
-- Dependencies: 178
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 193 (class 1255 OID 8743577)
-- Name: sp_get_list_users(integer, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION sp_get_list_users(page integer, nb_res integer, OUT id integer, OUT username character varying, OUT email character varying) RETURNS SETOF record
    LANGUAGE sql
    AS $_$

SELECT u.id, u.username, u.email
FROM users u
WHERE u.deleted_at IS NULL
ORDER BY u.created_at
LIMIT $2 OFFSET ($1 - 1) * $2
$_$;


--
-- TOC entry 192 (class 1255 OID 8743571)
-- Name: sp_get_social_user(character varying, character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION sp_get_social_user(social_id character varying, social_name character varying, OUT id integer, OUT username character varying, OUT email character varying, OUT avatar_url character varying, OUT deleted_at timestamp without time zone) RETURNS SETOF record
    LANGUAGE sql
    AS $_$

SELECT u.id, u.username, u.email, u.avatar_url, u.deleted_at
FROM users u
JOIN users_social_networks usn ON u.id = usn.user_id
WHERE usn.value = $1 AND usn.social_network_id = (SELECT id FROM social_networks WHERE name = $2)
$_$;


--
-- TOC entry 191 (class 1255 OID 8743554)
-- Name: sp_set_social_user(integer, character varying, character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION sp_set_social_user(user_id integer, social_name character varying, social_id character varying) RETURNS void
    LANGUAGE sql
    AS $_$
INSERT INTO users_social_networks (user_id, social_network_id, value) VALUES ($1, (SELECT id FROM social_networks WHERE name = $2), $3);
$_$;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 173 (class 1259 OID 8743474)
-- Name: roles; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE roles (
    id integer NOT NULL,
    name character varying NOT NULL
);


--
-- TOC entry 172 (class 1259 OID 8743472)
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 1990 (class 0 OID 0)
-- Dependencies: 172
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE roles_id_seq OWNED BY roles.id;


--
-- TOC entry 171 (class 1259 OID 8743466)
-- Name: social_networks; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE social_networks (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


--
-- TOC entry 170 (class 1259 OID 8743464)
-- Name: social_networks_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE social_networks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 1991 (class 0 OID 0)
-- Dependencies: 170
-- Name: social_networks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE social_networks_id_seq OWNED BY social_networks.id;


--
-- TOC entry 175 (class 1259 OID 8743490)
-- Name: users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    username character varying NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255),
    salt character varying(255),
    avatar_url character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    deleted_at timestamp without time zone
);


--
-- TOC entry 174 (class 1259 OID 8743488)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 1992 (class 0 OID 0)
-- Dependencies: 174
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- TOC entry 177 (class 1259 OID 8743506)
-- Name: users_roles; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users_roles (
    user_id integer NOT NULL,
    role_id integer NOT NULL
);


--
-- TOC entry 176 (class 1259 OID 8743501)
-- Name: users_social_networks; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users_social_networks (
    user_id integer NOT NULL,
    social_network_id integer NOT NULL,
    value character varying(255) NOT NULL
);


--
-- TOC entry 1853 (class 2604 OID 8743477)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY roles ALTER COLUMN id SET DEFAULT nextval('roles_id_seq'::regclass);


--
-- TOC entry 1852 (class 2604 OID 8743469)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY social_networks ALTER COLUMN id SET DEFAULT nextval('social_networks_id_seq'::regclass);


--
-- TOC entry 1854 (class 2604 OID 8743493)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- TOC entry 1858 (class 2606 OID 8743482)
-- Name: roles_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- TOC entry 1856 (class 2606 OID 8743471)
-- Name: social_networks_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY social_networks
    ADD CONSTRAINT social_networks_pkey PRIMARY KEY (id);


--
-- TOC entry 1860 (class 2606 OID 8743532)
-- Name: users_email_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_email_key UNIQUE (email);


--
-- TOC entry 1862 (class 2606 OID 8743498)
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 1870 (class 2606 OID 8743510)
-- Name: users_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users_roles
    ADD CONSTRAINT users_roles_pkey PRIMARY KEY (user_id, role_id);


--
-- TOC entry 1866 (class 2606 OID 8743505)
-- Name: users_social_networks_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users_social_networks
    ADD CONSTRAINT users_social_networks_pkey PRIMARY KEY (user_id, social_network_id);


--
-- TOC entry 1868 (class 2606 OID 8743545)
-- Name: users_social_networks_value_social_network_id_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users_social_networks
    ADD CONSTRAINT users_social_networks_value_social_network_id_key UNIQUE (value, social_network_id);


--
-- TOC entry 1864 (class 2606 OID 8743500)
-- Name: users_username_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_username_key UNIQUE (username);


--
-- TOC entry 1874 (class 2606 OID 8743516)
-- Name: users_roles_role_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users_roles
    ADD CONSTRAINT users_roles_role_id_fkey FOREIGN KEY (role_id) REFERENCES roles(id);


--
-- TOC entry 1873 (class 2606 OID 8743511)
-- Name: users_roles_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users_roles
    ADD CONSTRAINT users_roles_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- TOC entry 1872 (class 2606 OID 8743526)
-- Name: users_social_networks_social_network_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users_social_networks
    ADD CONSTRAINT users_social_networks_social_network_id_fkey FOREIGN KEY (social_network_id) REFERENCES social_networks(id);


--
-- TOC entry 1871 (class 2606 OID 8743521)
-- Name: users_social_networks_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users_social_networks
    ADD CONSTRAINT users_social_networks_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- TOC entry 1988 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2015-08-04 09:24:40

--
-- PostgreSQL database dump complete
--

--DATA

INSERT INTO social_networks (name) VALUES ('facebook');
INSERT INTO social_networks (name) VALUES ('twitter');
INSERT INTO roles (name) VALUES ('admin');
