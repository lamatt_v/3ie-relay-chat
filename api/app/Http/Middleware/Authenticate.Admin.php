<?php namespace App\Http\Middleware;

use App\Http\DAL\UserDAL;
use App\Http\Helpers\JWTException;
use App\Http\Helpers\JWTHelper;
use Illuminate\Contracts\Routing\Middleware;
use Closure;

class AuthenticateAdmin {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  Closure $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$token = $request->header(Authenticate::TOKEN_KEY);

		if (is_null($token))
		{
			abort(401, "Unauthorized");
		}

		try
		{
			$data = JWTHelper::decode($token);
			$user = UserDAL::find($data[JWTHelper::USER_KEY]);
			if (is_null($user) || !is_null($user->getDeletedAt()) || !UserDAL::isAdmin($user->getId()))
			{
				abort(401, "Unauthorized");
			}
			else
			{
				$request->merge(["user" => $user]);
			}
		}
		catch (JWTException $e)
		{
			abort(400, $e->getMessage());
		}

		return $next($request);
	}
}
