<?php
/**
 * Created by PhpStorm.
 * User: valentin.lamatte
 * Date: 13/02/16
 * Time: 23:20
 */

namespace App\Http\Controllers;

use App\Http\Business\ImageBusiness;
use Illuminate\Http\Request;

class ImageController extends AbstractController
{
    public function postImage(Request $request, $channel_id)
    {
        $business = new ImageBusiness($request->all());

        $tmpFile = $request->getContent();
        $business->setParam('channel_id', $channel_id);
        $business->setParam('media_file', $tmpFile);

        return $business->postImage();
    }
}