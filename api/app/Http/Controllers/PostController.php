<?php
/**
 * Created by PhpStorm.
 * User: valentin.lamatte
 * Date: 12/02/16
 * Time: 14:12
 */

namespace App\Http\Controllers;

use App\Http\Business\AbstractBusiness;
use App\Http\Business\PostBusiness;
use App\Http\DBO\Post;
use Illuminate\Http\Request;

class PostController extends AbstractController
{
    public function getFromChannel(Request $request, $channel_id)
    {
        $business = new PostBusiness($request->all());
        $business->setParam('channel_id', $channel_id);
        return $business->getFromChannel();
    }

    public function create(Request $request, $channel_id)
    {
        $business = new PostBusiness($request->all());
        $business->setParam('channel_id', $channel_id);
        return $business->create();
    }
}