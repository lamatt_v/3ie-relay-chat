<?php
/**
 * Created by PhpStorm.
 * User: adrien.gandarias
 * Date: 08/04/2015
 * Time: 10:16
 */

namespace App\Http\Controllers;


use App\Http\Business\UserBusiness;
use App\Http\DBO\User;
use Illuminate\Http\Request;

class UserController extends AbstractController
{
    public function create(Request $request)
    {
        $business = new UserBusiness($request->all());
        return $business->create();
    }

    public function login(Request $request)
    {
        $business = new UserBusiness($request->all());
        return $business->login();
    }

    public function getProfile(Request $request)
    {
        $business = new UserBusiness($request->all());
        return $business->getProfile();
    }

    public function changePassword(Request $request)
    {
        $business = new UserBusiness($request->all());
        return $business->changePassword();
    }

    public function facebookLogin(Request $request)
    {
        $business = new UserBusiness($request->all());
        return $business->facebookLogin();
    }

    public function twitterLogin(Request $request)
    {
        $business = new UserBusiness($request->all());
        return $business->twitterLogin();
    }

    public function editProfile(Request $request)
    {
        $business = new UserBusiness($request->all());
        return $business->editProfile();
    }

    public function editAvatar(Request $request)
    {
        $business = new UserBusiness($request->all());
        $tmpFile = $request->getContent();
        $business->setParam('media_file', $tmpFile);

        return $business->editAvatar();
    }

    public function getList(Request $request)
    {
        $params = $request->route()->parameters();
        $params["user"] = $request->get("user");
        $business = new UserBusiness($params);
        return $business->getList();
    }

    public function get(Request $request)
    {
        $params = $request->route()->parameters();
        $params["user"] = $request->get("user");
        $business = new UserBusiness($params);
        return $business->get();
    }

    public function authAdmin(Request $request)
    {
        //authAdmin middleware called before this method
        return [ "code" => 200, "message" => "OK" ];
    }

    public function count(Request $request)
    {
        $business = new UserBusiness($request->all());
        return $business->count();
    }

    public function edit(Request $request)
    {
        $business = new UserBusiness($request->all());
        return $business->edit();
    }

    public function delete(Request $request)
    {
        $business = new UserBusiness($request->all());
        return $business->delete();
    }
}