<?php
/**
 * Created by PhpStorm.
 * User: Deniz
 * Date: 12/02/2016
 * Time: 15:42
 */

namespace App\Http\Controllers;

use App\Http\Business\ChannelBusiness;
use App\Http\DAL\Models\Channel;
use Illuminate\Http\Request;
class ChannelController extends AbstractController
{
    public function create(Request $request)
    {
        $business = new ChannelBusiness($request->all());
        return $business->create();
    }
    public function getList(Request $request)
    {
        $business = new ChannelBusiness($request->all());
        return $business->getList();
    }

    public function login(Request $request, $channel_id)
    {
        $business = new ChannelBusiness($request->all());
        $business->setParam('channel_id', $channel_id);
        return $business->login();
    }
}