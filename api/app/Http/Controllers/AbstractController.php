<?php
/**
 * Created by PhpStorm.
 * User: adrien.gandarias
 * Date: 08/04/2015
 * Time: 10:09
 */

namespace App\Http\Controllers;


abstract class AbstractController extends Controller
{

    public function __construct()
    {
        header('Content-type: application/json; charset=UTF-8');
    }

    /**
     * Check javascript token provided to authenticate user
     * Either returns a User model or aborts application
     */
    public function authenticate()
    {
        //Code to check if user is authenticated or not

//        if ($user instanceof User)
//            return $user;
//        else
        \App::abort(401, 'AUTHENTICATION_REQUIRED');
    }

    public function admin()
    {
        //Code to check if authenticated as an admin or not

//        $user = $this->authenticate();
//
//        if (UserService::isAdmin($user))
//            return $user;
//        else
        \App::abort(401, 'UNAUTHORIZED');
    }

}