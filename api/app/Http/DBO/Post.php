<?php
/**
 * Created by PhpStorm.
 * User: valentin.lamatte
 * Date: 12/02/16
 * Time: 14:24
 */

namespace App\Http\DBO;

use Illuminate\Database\Eloquent\Model;


class Post extends AbstractModel
{
    #region attributes

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $content;

    /**
     * @var int
     */
    private $channel_id;

    /**
     * @var string
     */
    private $created_at;

    /**
     * @var int
     */
    private $user_id;

    /**
     * @var int
     */
    private $image_id;

    #endregion

    function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'content' => $this->content,
            'channel_id' => $this->channel_id,
            'created_at' => $this->created_at,
            'user_id' => $this->user_id,
            'image_id' => $this->image_id
        ];
    }

    /**
     * @return int
     */
    public function getImageId()
    {
        return $this->image_id;
    }

    /**
     * @param int $image_id
     */
    public function setImageId($image_id)
    {
        $this->image_id = $image_id;
    }

    #region getters_setters

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return int
     */
    public function getChannelId()
    {
        return $this->channel_id;
    }

    /**
     * @param int $channel_id
     */
    public function setChannelId($channel_id)
    {
        $this->channel_id = $channel_id;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    #endregion
}