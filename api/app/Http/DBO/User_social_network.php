<?php
/**
 * Created by JetBrains PhpStorm.
 * User: cedric.schneider
 * Date: 16/07/15
 * Time: 17:46
 * To change this template use File | Settings | File Templates.
 */
namespace App\Http\DBO;

class User_social_network extends AbstractModel
{
    #region attributes
    /**
     * @var int
     */
    private $user_id;

    /**
     * @var int
     */
    private $social_network_id;

    /**
     * @var string
     */
    private $value;
    #endregion
    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    public function jsonSerialize()
    {
        return [
            "user_id" => $this->user_id,
            "social_network_id" => $this->social_network_id,
            "value" => $this->value
        ];
    }

    #region getters_setters
    /**
     * @param int $social_network_id
     */
    public function setSocialNetworkId($social_network_id)
    {
        $this->social_network_id = $social_network_id;
    }

    /**
     * @return int
     */
    public function getSocialNetworkId()
    {
        return $this->social_network_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
    #endregion
}