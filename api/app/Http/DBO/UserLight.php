<?php
/**
 * Created by PhpStorm.
 * User: cedric.schneider
 * Date: 21/07/2015
 * Time: 09:28
 */
namespace App\Http\DBO;

class UserLight extends AbstractModel
{
    #region attributes
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $email;

    /**
     * @var bool
     */
    private $is_current_user;
    #endregion
    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    function jsonSerialize()
    {
        return [
            "id" => $this->id,
            "username" => $this->username,
            "email" => $this->email,
            "is_current_user" => $this->is_current_user
        ];
    }

    #region getters_setters
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return boolean
     */
    public function isIsCurrentUser()
    {
        return $this->is_current_user;
    }

    /**
     * @param boolean $is_current_user
     */
    public function setIsCurrentUser($is_current_user)
    {
        $this->is_current_user = $is_current_user;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
    #endregion
}