<?php
/**
 * Created by JetBrains PhpStorm.
 * User: cedric.schneider
 * Date: 16/07/15
 * Time: 16:59
 * To change this template use File | Settings | File Templates.
 */

namespace App\Http\DBO;

class Role extends AbstractModel
{
    #region attributes
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;
    #endregion

    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    public function jsonSerialize()
    {
        return [
            "id" => $this->id,
            "name" => $this->name
        ];
    }

    #region getters_setters
    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    #endregion
}