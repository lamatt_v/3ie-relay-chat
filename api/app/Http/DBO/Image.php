<?php
/**
 * Created by PhpStorm.
 * User: valentin.lamatte
 * Date: 13/02/16
 * Time: 21:59
 */

namespace App\Http\DBO;

use Illuminate\Database\Eloquent\Model;

class Image extends AbstractModel
{

    #region attributes
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $created_at;

    function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'url' => is_null($this->url) ? null : ("http://" . $_SERVER['SERVER_NAME'] . $this->url)
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }
}