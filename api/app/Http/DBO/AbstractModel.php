<?php
/**
 * Created by PhpStorm.
 * User: adrien.gandarias
 * Date: 08/04/2015
 * Time: 10:00
 */

namespace App\Http\DBO;


use Illuminate\Database\Eloquent\Model;

abstract class AbstractModel implements \JsonSerializable {
    /**
     * @return string
     */
    public function __toString()
    {
        return json_encode($this);
    }
}