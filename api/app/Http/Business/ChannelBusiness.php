<?php
/**
 * Created by PhpStorm.
 * User: Deniz
 * Date: 12/02/2016
 * Time: 15:47
 */

namespace App\Http\Business;

use App\Http\Business\AbstractBusiness;
use App\Http\DAL\ChannelDAL;
use App\Http\DAL\Models\Channel;

class ChannelBusiness extends AbstractBusiness
{
    public function getList(){
        return ChannelDAL::getList();
    }
    public function create()
    {
        if(!$this->hasParam(["name"]))
        {
            abort(400, "MISSING_PARAMETERS");
        }
        $name = $this->getParam("name");
        $password = "";
        $hasPassword = false;
        if($this->hasParam(["password"]))
        {
            $hasPassword = true;
            $password = $this->getParam("password");
        }
        $check_channel = ChannelDAL::findByName($name);
        if (!is_null($check_channel))
        {
            abort(409, "NAME_ALREADY_USED");
        }
        $channel = new \App\Http\DBO\Channel();
        $channel->setName($name);
        if ($hasPassword)
            $channel->setPassword($password);
        ChannelDAL::create($channel);
    }
    public function login(){

        if (!$this->hasParam(['channel_id']))
        {

            abort(400, "MISSING_PARAMETERS");
        }
        $id = $this->getParam("channel_id");
        $channel = ChannelDAL::find($id);
        if (is_null($channel))
        {
            abort(404, "CHANNEL_NOT_FOUND");
        }
        if(is_null($this->getParam('password')) && !(is_null($channel->getPassword())))
        {
            abort(400, "MISSING_PASSWORD");
        }
        if(!is_null($channel->getPassword()) && ($this->getParam('password')!=$channel->getPassword()))
        {
            abort(422, 'WRONG_PASSWORD');
        }
        // return ChannelDal::getPosts($id);
        
    }
}