<?php
/**
 * Created by PhpStorm.
 * User: adrien.gandarias
 * Date: 08/04/2015
 * Time: 10:01
 */

namespace App\Http\Business;

class AbstractBusiness {
    /**
     * @var array
     */
    private $parameters = [];

    /**
     * @param array $parametersSource
     */
    public function __construct(array $parametersSource)
    {
        $this->parameters = $parametersSource;
    }

    /**
     * @param string $name
     * @param mixed  $default
     * @return mixed
     */
    protected function getParam($name, $default = null)
    {
        return array_key_exists($name, $this->parameters) ? $this->parameters[$name] : $default;
    }

    /**
     * @param string $name
     * @param mixed  $value
     */
    public function setParam($name, $value)
    {
        $this->parameters[$name] = $value;
    }

    /**
     * @param string[] $params
     * @return bool
     */
    public function hasParam(array $params)
    {
        foreach ($params as $p)
        {
            if (!array_key_exists($p, $this->parameters))
                return false;
        }
        return true;
    }
}