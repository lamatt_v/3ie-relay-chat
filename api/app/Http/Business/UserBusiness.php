<?php
/**
 * Created by PhpStorm.
 * User: adrien.gandarias
 * Date: 08/04/2015
 * Time: 10:14
 */

namespace App\Http\Business;


use App\Http\DAL\UserDAL;
use App\Http\DBO\Social_network;
use App\Http\DBO\User;
use App\Http\DBO\UserLight;
use App\Http\Helpers\JWTHelper;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Gd\Imagine;
use Imagine\Image\Point;

class UserBusiness extends AbstractBusiness
{
    const IMAGE_WIDTH = 500;
    const IMAGE_HEIGHT = 500;
    const IMAGE_DIRECTORY = '/images/users/';

    public function create()
    {
        if (!$this->hasParam(["email", "username", "password"]))
        {
            abort(400, "MISSING_PARAMETERS");
        }

        if (!defined("CRYPT_BLOWFISH") || !CRYPT_BLOWFISH)
        {
            abort(500, "SERVER_ERROR");
        }

        $email = $this->getParam("email");
        $username = $this->getParam("username");
        $password = $this->getParam("password");

        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false)
        {
            abort(422, 'BAD_EMAIL');
        }

        $check_user = UserDAL::findByEmail($email);
        if (!is_null($check_user))
        {
            abort(409, "EMAIL_ALREADY_USED");
        }

        $check_user = UserDAL::findByUsername($username);
        if (!is_null($check_user))
        {
            abort(409, "USERNAME_ALREADY_USED");
        }

        $user = new User();
        $user->setEmail($email);
        $user->setUsername($username);
        $user->setHashPassword($password);

        $user = UserDAL::create($user);

        $token = JWTHelper::encode([ JWTHelper::USER_KEY => $user->getId() ] );
        return [ "token" => $token ];
    }

    public function login()
    {
        if (!$this->hasParam(["email", "password"]))
        {
            abort(400, "MISSING_PARAMETERS");
        }

        if (!defined("CRYPT_BLOWFISH") || !CRYPT_BLOWFISH)
        {
            abort(500, "SERVER_ERROR");
        }

        $user = UserDAL::findByEmail($this->getParam("email"));
        if (is_null($user) || !is_null($user->getDeletedAt()))
        {
            abort(401, 'INVALID_CREDENTIALS');
        }

        $password = crypt($this->getParam("password"), sprintf('$2y$%02d$', User::ROUND) . $user->getSalt());

        if ($password != $user->getPassword())
        {
            abort(401, 'INVALID_CREDENTIALS');
        }

        $token = JWTHelper::encode([ JWTHelper::USER_KEY => $user->getId() ] );
        return [ "token" => $token ];
    }

    public function getProfile()
    {
        return $this->getParam("user");
    }

    public function changePassword()
    {
        if (!$this->hasParam(["password", "new_password"]))
        {
            abort(400, "MISSING_PARAMETERS");
        }

        if (!defined("CRYPT_BLOWFISH") || !CRYPT_BLOWFISH)
        {
            abort(500, "SERVER_ERROR");
        }

        $user = $this->getParam("user");
        /** @var $user User */

        $old_password = crypt($this->getParam("password"), sprintf('$2y$%02d$', User::ROUND) . $user->getSalt());
        if ($old_password != $user->getPassword())
        {
            abort(401, 'INVALID_PASSWORD');
        }

        $password = $this->getParam("new_password");
        $user->setHashPassword($password);
        if (UserDAL::edit($user))
            return $user;
        else
            abort(500);
    }

    public function facebookLogin()
    {
        if (!$this->hasParam(["username", "facebook_id", "email"]))
        {
            abort(400, "MISSING_PARAMETERS");
        }

        $facebook_id = $this->getParam("facebook_id");
        $username = $this->getParam("username");
        $email = $this->getParam("email");

        $user = UserDAL::getSocialUser($facebook_id, Social_network::FACEBOOK);
        if (is_null($user))
        {
            $check_user = UserDAL::findByEmail($email);
            if (!is_null($check_user))
            {
                abort(409, "EMAIL_ALREADY_USED");
            }

            $i = 0;
            while (!is_null(UserDAL::findByUsername($username))) //check username
                $username .= ++$i;

            $user = new User();
            $user->setId(0);
            $user->setUsername($username);
            $user->setEmail($email);
        }
        else if (!is_null($user->getDeletedAt()))
            abort(401, 'INVALID_CREDENTIALS');

        $update_user = false;
        //update avatar
        if ($this->hasParam(['avatar_url']) && $this->getParam('avatar_url'))
        {
            if (!is_null($user->getAvatarUrl()) && file_exists(public_path() . $user->getAvatarUrl()))
                unlink(public_path() . $user->getAvatarUrl());

            $media_file = file_get_contents($this->getParam("avatar_url"));
            if (strlen($media_file) <= 1500000 && strlen($media_file) > 0)
            {
                $name = sha1(uniqid(mt_rand(), true));
                $fileInfo = finfo_open();
                $sMimeType = finfo_buffer($fileInfo, $media_file, FILEINFO_MIME_TYPE);
                finfo_close($fileInfo);
                if (in_array($sMimeType, ['image/jpeg', 'image/jpg', 'image/png']))
                {
                    $path = public_path() . self::IMAGE_DIRECTORY;
                    if (!is_dir($path))
                    {
                        mkdir($path, 0777, true);
                    }

                    $tmp_path = $path . uniqid();
                    $this->write_file($tmp_path, $media_file);

                    $this->save_image($tmp_path, $path, $name);

                    unlink($tmp_path);

                    $user->setAvatarUrl(self::IMAGE_DIRECTORY . $name . ".jpg");
                    $update_user = true;
                }
            }
        }

        if ($user->getId() == 0)
        {
            $user = UserDAL::create($user);
            UserDAL::setSocialUser($facebook_id, Social_network::FACEBOOK, $user->getId());
        }
        else if ($update_user)
        {
            UserDAL::edit($user);
        }

        $token = JWTHelper::encode([ JWTHelper::USER_KEY => $user->getId() ] );
        return [ "token" => $token ];
    }

    public function twitterLogin()
    {
        if (!$this->hasParam(["username", "twitter_id", "email"]))
        {
            abort(400, "MISSING_PARAMETERS");
        }

        $twitter_id = $this->getParam("twitter_id");
        $username = $this->getParam("username");
        $email = $this->getParam("email");

        $user = UserDAL::getSocialUser($twitter_id, Social_network::TWITTER);
        if (is_null($user))
        {
            $check_user = UserDAL::findByEmail($email);
            if (!is_null($check_user))
            {
                abort(409, "EMAIL_ALREADY_USED");
            }

            $i = 0;
            while (!is_null(UserDAL::findByUsername($username))) //check username
                $username .= ++$i;

            $user = new User();
            $user->setId(0);
            $user->setUsername($username);
            $user->setEmail($email);
        }
        else if (!is_null($user->getDeletedAt()))
            abort(401, 'INVALID_CREDENTIALS');

        $update_user = false;
        //update avatar
        if ($this->hasParam(['avatar_url']) && $this->getParam('avatar_url'))
        {
            if (!is_null($user->getAvatarUrl()) && file_exists(public_path() . $user->getAvatarUrl()))
                unlink(public_path() . $user->getAvatarUrl());

            $media_file = file_get_contents($this->getParam("avatar_url"));
            if (strlen($media_file) <= 1500000 && strlen($media_file) > 0)
            {
                $name = sha1(uniqid(mt_rand(), true));
                $fileInfo = finfo_open();
                $sMimeType = finfo_buffer($fileInfo, $media_file, FILEINFO_MIME_TYPE);
                finfo_close($fileInfo);
                if (in_array($sMimeType, ['image/jpeg', 'image/jpg', 'image/png']))
                {
                    $path = public_path() . self::IMAGE_DIRECTORY;
                    if (!is_dir($path))
                    {
                        mkdir($path, 0777, true);
                    }

                    $tmp_path = $path . uniqid();
                    $this->write_file($tmp_path, $media_file);

                    $this->save_image($tmp_path, $path, $name);

                    unlink($tmp_path);

                    $user->setAvatarUrl(self::IMAGE_DIRECTORY . $name . ".jpg");
                    $update_user = true;
                }
            }
        }

        if ($user->getId() == 0)
        {
            $user = UserDAL::create($user);
            UserDAL::setSocialUser($twitter_id, Social_network::TWITTER, $user->getId());
        }
        else if ($update_user)
        {
            UserDAL::edit($user);
        }

        $token = JWTHelper::encode([ JWTHelper::USER_KEY => $user->getId() ] );
        return [ "token" => $token ];
    }

    public function editProfile()
    {
        if (!$this->hasParam(['email', 'username']))
        {
            abort(400, "MISSING_PARAMETERS");
        }

        $user = $this->getParam("user");
        /** @var $user User */

        $email = $this->getParam("email");
        $username = $this->getParam("username");

        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false)
        {
            abort(422, 'BAD_EMAIL');
        }

        $check_user = UserDAL::findByEmail($email);
        if (!is_null($check_user) && $check_user->getId() != $user->getId())
        {
            abort(409, "EMAIL_ALREADY_USED");
        }

        $check_user = UserDAL::findByUsername($username);
        if (!is_null($check_user) && $check_user->getId() != $user->getId())
        {
            abort(409, "USERNAME_ALREADY_USED");
        }

        $user->setEmail($email);
        $user->setUsername($username);

        if (UserDAL::edit($user))
            return $user;
        else
            abort(500);
    }

    public function editAvatar()
    {
        $user = $this->getParam("user");
        /** @var $user User */
        //upload
        $media_file = $this->getParam('media_file');

        if (!$media_file)
        {
            abort(400, "MISSING_PARAMETER");
        }

        if (strlen($media_file) > 1500000)
        {
            abort(401, "FILE_TOO_LARGE");
        }

        $name = sha1(uniqid(mt_rand(), true));
        $fileInfo = finfo_open();
        $sMimeType = finfo_buffer($fileInfo, $media_file, FILEINFO_MIME_TYPE);
        finfo_close($fileInfo);
        if (!in_array($sMimeType, ['image/jpeg', 'image/jpg', 'image/png'])) {
            abort(401, "INVALID_FILE");
        }

        if (!is_null($user->getAvatarUrl()) && file_exists(public_path() . $user->getAvatarUrl()))
            unlink(public_path() . $user->getAvatarUrl());

        $path = public_path() . self::IMAGE_DIRECTORY;
        if (!is_dir($path))
            mkdir($path, 0777, true);

        $tmp_path = $path . uniqid();
        $this->write_file($tmp_path, $media_file);

        $this->save_image($tmp_path, $path, $name);

        unlink($tmp_path);

        $user->setAvatarUrl(self::IMAGE_DIRECTORY . $name . ".jpg");

        if (UserDAL::edit($user))
            return $user;
        else
            abort(500);
    }

    private function write_file($path, $data)
    {
        if ($f = fopen($path, 'w'))
        {
            if (false !== fwrite($f, $data))
            {
                fflush($f);
                fclose($f);
            }
            else
                abort(500, "Can't write file");
        }
        else
            abort(500, "Can't open directory");
    }

    private function save_image($tmp_path, $path, $name)
    {
        $box = new Box(self::IMAGE_WIDTH, self::IMAGE_HEIGHT);
        $filename = $path . $name . ".jpg";
        $imagine = new Imagine();
        $image = $imagine->open($tmp_path);
        $srcBox = $image->getSize();

        if ($srcBox->getWidth() < self::IMAGE_WIDTH || $srcBox->getHeight() < self::IMAGE_HEIGHT)
        {
            if ($srcBox->getWidth() < $srcBox->getHeight())
            {
                $ratio = self::IMAGE_WIDTH / $srcBox->getWidth();
                $image->resize(new Box(self::IMAGE_WIDTH, $ratio * $srcBox->getHeight()));
            }
            else
            {
                $ratio = self::IMAGE_HEIGHT / $srcBox->getHeight();
                $image->resize(new Box($ratio * $srcBox->getWidth(), self::IMAGE_HEIGHT));
            }
            $image->save($filename);
            $srcBox = $image->getSize();
        }

        if ($srcBox->getWidth() > $srcBox->getHeight()) {
            $width  = $srcBox->getWidth()*($box->getHeight() / $srcBox->getHeight());
            $height =  $box->getHeight();
            //we center the crop in relation to the width
            $cropPoint = new Point((max($width - $box->getWidth(), 0))/2, 0);
        } else {
            $width  = $box->getWidth();
            $height =  $srcBox->getHeight()*($box->getWidth()/$srcBox->getWidth());
            //we center the crop in relation to the height
            $cropPoint = new Point(0, (max($height - $box->getHeight(),0))/2);
        }

        $image
            ->thumbnail(new Box($width, $height), ImageInterface::THUMBNAIL_OUTBOUND)
            ->crop($cropPoint, $box)
            ->save($filename);
    }

    public function getList()
    {
        $page = $this->getParam("page", 1);
        $limit = $this->getParam("limit", 20);
        $user =  $this->getParam("user");
        /** @var $user User */

        $list = UserDAL::getList($page, $limit);
        /** @var $list UserLight[] */
        foreach ($list as $u)
            $u->setIsCurrentUser($user->getId() == $u->getId());

        return $list;
    }

    public function get()
    {
        if (!$this->hasParam(['id']))
        {
            abort(400, "MISSING_PARAMETERS");
        }

        $id = $this->getParam("id");
        $user = UserDAL::find($id);
        if (is_null($user))
            abort(404, "USER_NOT_FOUND");
        else
            return $user;
    }

    public function count()
    {
        return [ "count" => UserDAL::count() ];
    }

    public function edit()
    {
        if (!$this->hasParam(['email', 'username', 'id']))
        {
            abort(400, "MISSING_PARAMETERS");
        }

        $email = $this->getParam("email");
        $username = $this->getParam("username");
        $id = $this->getParam("id");

        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false)
        {
            abort(422, 'BAD_EMAIL');
        }

        $user = UserDAL::find($id);
        if (is_null($user))
        {
            abort(404, "USER_NOT_FOUND");
        }

        $check_user = UserDAL::findByEmail($email);
        if (!is_null($check_user) && $check_user->getId() != $id)
        {
            abort(409, "EMAIL_ALREADY_USED");
        }

        $check_user = UserDAL::findByUsername($username);
        if (!is_null($check_user) && $check_user->getId() != $id)
        {
            abort(409, "USERNAME_ALREADY_USED");
        }

        $user->setEmail($email);
        $user->setUsername($username);

        if (UserDAL::edit($user))
            return $user;
        else
            abort(500, "SERVER_ERROR");
    }

    public function delete()
    {
        if (!$this->hasParam(['id']))
        {
            abort(400, "MISSING_PARAMETERS");
        }

        $id = $this->getParam("id");
        if (is_null(UserDAL::find($id)))
        {
            abort(404, "USER_NOT_FOUND");
        }

        $user = $this->getParam("user");
        if ($id == $user->getId())
        {
            abort(500, "CANT_DELETE_CURRENT_USER");
        }

        if (UserDAL::delete($id))
            return ['id' => $id];
        else
            abort(500, "SERVER_ERROR");
    }
}