<?php
/**
 * Created by PhpStorm.
 * User: valentin.lamatte
 * Date: 13/02/16
 * Time: 22:22
 */

namespace App\Http\Business;


use App\Http\DAL\ImageDAL;
use App\Http\DBO\Image;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Gd\Imagine;
use Imagine\Image\Point;
use App\Http\DAL\PostDAL;
use App\Http\DBO\Post;

class ImageBusiness extends AbstractBusiness
{

    const IMAGE_WIDTH = 500;
    const IMAGE_HEIGHT = 500;
    const IMAGE_DIRECTORY = '/images/posts/';

    public function create($image_url)
    {
        $image = new Image();
        $image->setUrl($image_url);

        $image = ImageDAL::create($image);

        return $image;
    }

    public function postImage()
    {
        $media_file = $this->getParam('media_file');

        if (!$media_file)
        {
            abort(400, "MISSING_PARAMETER");
        }

        if (strlen($media_file) > 1500000)
        {
            abort(401, "FILE_TOO_LARGE");
        }

        $name = sha1(uniqid(mt_rand(), true));
        $fileInfo = finfo_open();
        $sMimeType = finfo_buffer($fileInfo, $media_file, FILEINFO_MIME_TYPE);
        finfo_close($fileInfo);
        if (!in_array($sMimeType, ['image/jpeg', 'image/jpg', 'image/png'])) {
            abort(401, "INVALID_FILE");
        }

        $path = public_path() . self::IMAGE_DIRECTORY;
        if (!is_dir($path))
            mkdir($path, 0777, true);

        $tmp_path = $path . uniqid();
        $this->write_file($tmp_path, $media_file);

        $this->save_image($tmp_path, $path, $name);

        unlink($tmp_path);

        $image = new Image();

        $image->setUrl(self::IMAGE_DIRECTORY . $name . ".jpg");

        $image = ImageDAL::create($image);

        $channel_id = $this->getParam("channel_id");
        $content = "";

        $user = $this->getParam("user");


        $post = new Post();
        $post->setChannelId($channel_id);
        $post->setContent($content);
        $post->setUserId($user->getId());
        $post->setImageId($image->getId());

        return PostDAL::create($post);
    }

    private function write_file($path, $data)
    {
        if ($f = fopen($path, 'w'))
        {
            if (false !== fwrite($f, $data))
            {
                fflush($f);
                fclose($f);
            }
            else
                abort(500, "Can't write file");
        }
        else
            abort(500, "Can't open directory");
    }

    private function save_image($tmp_path, $path, $name)
    {
        $box = new Box(self::IMAGE_WIDTH, self::IMAGE_HEIGHT);
        $filename = $path . $name . ".jpg";
        $imagine = new Imagine();
        $image = $imagine->open($tmp_path);
        $srcBox = $image->getSize();

        if ($srcBox->getWidth() < self::IMAGE_WIDTH || $srcBox->getHeight() < self::IMAGE_HEIGHT)
        {
            if ($srcBox->getWidth() < $srcBox->getHeight())
            {
                $ratio = self::IMAGE_WIDTH / $srcBox->getWidth();
                $image->resize(new Box(self::IMAGE_WIDTH, $ratio * $srcBox->getHeight()));
            }
            else
            {
                $ratio = self::IMAGE_HEIGHT / $srcBox->getHeight();
                $image->resize(new Box($ratio * $srcBox->getWidth(), self::IMAGE_HEIGHT));
            }
            $image->save($filename);
            $srcBox = $image->getSize();
        }

        if ($srcBox->getWidth() > $srcBox->getHeight()) {
            $width  = $srcBox->getWidth()*($box->getHeight() / $srcBox->getHeight());
            $height =  $box->getHeight();
            //we center the crop in relation to the width
            $cropPoint = new Point((max($width - $box->getWidth(), 0))/2, 0);
        } else {
            $width  = $box->getWidth();
            $height =  $srcBox->getHeight()*($box->getWidth()/$srcBox->getWidth());
            //we center the crop in relation to the height
            $cropPoint = new Point(0, (max($height - $box->getHeight(),0))/2);
        }

        $image
            ->thumbnail(new Box($width, $height), ImageInterface::THUMBNAIL_OUTBOUND)
            ->crop($cropPoint, $box)
            ->save($filename);
    }
}