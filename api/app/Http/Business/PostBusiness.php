<?php
/**
 * Created by PhpStorm.
 * User: valentin.lamatte
 * Date: 12/02/16
 * Time: 14:14
 */

namespace App\Http\Business;

use App\Http\DAL\PostDAL;
use App\Http\DBO\Post;
use Imagine\Image\Point;

class PostBusiness extends AbstractBusiness
{
    public function getFromChannel()
    {
        if (!$this->hasParam(["nb_res", "min_date"]))
        {
            abort(400, "MISSING_PARAMETERS");
        }

        $channel_id = $this->getParam("channel_id");
        $nb_res = $this->getParam("nb_res");
        $min_date = $this->getParam("min_date");
        return PostDAL::getFromChannel($channel_id, $nb_res, $min_date);
    }

    public function create()
    {
        if (!$this->hasParam(["content", "channel_id"]))
        {
            abort(400, "MISSING_PARAMETERS");
        }

        $channel_id = $this->getParam("channel_id");
        $content = $this->getParam("content");
        /**
         * @var $user \App\Http\DBO\User
         */
        $user = $this->getParam("user");


        $post = new Post();
        $post->setChannelId($channel_id);
        $post->setContent($content);
        $post->setUserId($user->getId());


        return PostDAL::create($post);
    }
}