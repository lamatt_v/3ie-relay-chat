<?php
/**
 * Class SP_Set_social_user
 *
 * This is an auto-generated class.
 * DO NOT EDIT IT ! It will be rewritten at next generation.
 */

namespace App\Http\DAL\stored_procedures;

class SP_Set_social_user extends \Mrblackus\LaravelStoredprocedures\SPModel
{
  /**
   * @return array
   */
  public static function execute($user_id, $social_name, $social_id)
  {
    $pdo   = \DB::connection()->getPdo();
    $query = $pdo->prepare('SELECT * FROM sp_set_social_user(:user_id, :social_name, :social_id)');

    self::bindPDOValue($query, ':user_id', $user_id);
    self::bindPDOValue($query, ':social_name', $social_name);
    self::bindPDOValue($query, ':social_id', $social_id);

    $query->execute();
    $res = array();

    foreach ($query->fetchAll(\PDO::FETCH_COLUMN) as $v)
    {
      $res[] = $v;
    }

    return $res;
  }
}