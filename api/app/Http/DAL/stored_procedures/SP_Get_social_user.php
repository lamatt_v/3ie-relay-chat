<?php
/**
 * Class SP_Get_social_user
 *
 * This is an auto-generated class.
 * DO NOT EDIT IT ! It will be rewritten at next generation.
 */

namespace App\Http\DAL\stored_procedures;

class SP_Get_social_user extends \Mrblackus\LaravelStoredprocedures\SPModel
{
  protected $id;

  public function getId()
  {
    return $this->id;
  }

  public function setId($id)
  {
    $this->id = $id;
  }
  protected $username;

  public function getUsername()
  {
    return $this->username;
  }

  public function setUsername($username)
  {
    $this->username = $username;
  }
  protected $email;

  public function getEmail()
  {
    return $this->email;
  }

  public function setEmail($email)
  {
    $this->email = $email;
  }
  protected $avatar_url;

  public function getAvatar_url()
  {
    return $this->avatar_url;
  }

  public function setAvatar_url($avatar_url)
  {
    $this->avatar_url = $avatar_url;
  }
  protected $deleted_at;

  public function getDeleted_at()
  {
    return $this->deleted_at;
  }

  public function setDeleted_at($deleted_at)
  {
    $this->deleted_at = $deleted_at;
  }
  /**
   * @return SP_Get_social_user[]
   */
  public static function execute($social_id, $social_name)
  {
    $pdo   = \DB::connection()->getPdo();
    $query = $pdo->prepare('SELECT * FROM sp_get_social_user(:social_id, :social_name)');

    self::bindPDOValue($query, ':social_id', $social_id);
    self::bindPDOValue($query, ':social_name', $social_name);

    $query->execute();
    $res = array();

    foreach ($query->fetchAll(\PDO::FETCH_ASSOC) as $v)
    {
      $object = new SP_Get_social_user();
      self::hydrate($object, $v);
      $res[] = $object;
    }

    return $res;
  }
}