<?php
/**
 * Class SP_Get_list_channels
 *
 * This is an auto-generated class.
 * DO NOT EDIT IT ! It will be rewritten at next generation.
 */

namespace App\Http\DAL\stored_procedures;

class SP_Get_list_channels extends \Mrblackus\LaravelStoredprocedures\SPModel
{
  protected $id;

  public function getId()
  {
    return $this->id;
  }

  public function setId($id)
  {
    $this->id = $id;
  }
  protected $name;

  public function getName()
  {
    return $this->name;
  }

  public function setName($name)
  {
    $this->name = $name;
  }
  /**
   * @return SP_Get_list_channels[]
   */
  public static function execute()
  {
    $pdo   = \DB::connection()->getPdo();
    $query = $pdo->prepare('SELECT * FROM sp_get_list_channels()');

    $query->execute();
    $res = array();

    foreach ($query->fetchAll(\PDO::FETCH_ASSOC) as $v)
    {
      $object = new SP_Get_list_channels();
      self::hydrate($object, $v);
      $res[] = $object;
    }

    return $res;
  }
}