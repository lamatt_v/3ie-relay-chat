<?php
/**
 * Class SP_Get_list_posts
 *
 * This is an auto-generated class.
 * DO NOT EDIT IT ! It will be rewritten at next generation.
 */

namespace App\Http\DAL\stored_procedures;

class SP_Get_list_posts extends \Mrblackus\LaravelStoredprocedures\SPModel
{
  protected $user_id;

  public function getUser_id()
  {
    return $this->user_id;
  }

  public function setUser_id($user_id)
  {
    $this->user_id = $user_id;
  }
  protected $post_content;

  public function getPost_content()
  {
    return $this->post_content;
  }

  public function setPost_content($post_content)
  {
    $this->post_content = $post_content;
  }
  protected $created_at;

  public function getCreated_at()
  {
    return $this->created_at;
  }

  public function setCreated_at($created_at)
  {
    $this->created_at = $created_at;
  }
  protected $image_url;

  public function getImage_url()
  {
    return $this->image_url;
  }

  public function setImage_url($image_url)
  {
    $this->image_url = $image_url;
  }
  /**
   * @return SP_Get_list_posts[]
   */
  public static function execute($date_min, $nb_res, $channel_id)
  {
    $pdo   = \DB::connection()->getPdo();
    $query = $pdo->prepare('SELECT * FROM sp_get_list_posts(:date_min, :nb_res, :channel_id)');

    self::bindPDOValue($query, ':date_min', $date_min);
    self::bindPDOValue($query, ':nb_res', $nb_res);
    self::bindPDOValue($query, ':channel_id', $channel_id);

    $query->execute();
    $res = array();

    foreach ($query->fetchAll(\PDO::FETCH_ASSOC) as $v)
    {
      $object = new SP_Get_list_posts();
      self::hydrate($object, $v);
      $res[] = $object;
    }

    return $res;
  }
}