<?php
/**
 * Class SP_Get_list_users
 *
 * This is an auto-generated class.
 * DO NOT EDIT IT ! It will be rewritten at next generation.
 */

namespace App\Http\DAL\stored_procedures;

class SP_Get_list_users extends \Mrblackus\LaravelStoredprocedures\SPModel
{
  protected $id;

  public function getId()
  {
    return $this->id;
  }

  public function setId($id)
  {
    $this->id = $id;
  }
  protected $username;

  public function getUsername()
  {
    return $this->username;
  }

  public function setUsername($username)
  {
    $this->username = $username;
  }
  protected $email;

  public function getEmail()
  {
    return $this->email;
  }

  public function setEmail($email)
  {
    $this->email = $email;
  }
  /**
   * @return SP_Get_list_users[]
   */
  public static function execute($page, $nb_res)
  {
    $pdo   = \DB::connection()->getPdo();
    $query = $pdo->prepare('SELECT * FROM sp_get_list_users(:page, :nb_res)');

    self::bindPDOValue($query, ':page', $page);
    self::bindPDOValue($query, ':nb_res', $nb_res);

    $query->execute();
    $res = array();

    foreach ($query->fetchAll(\PDO::FETCH_ASSOC) as $v)
    {
      $object = new SP_Get_list_users();
      self::hydrate($object, $v);
      $res[] = $object;
    }

    return $res;
  }
}