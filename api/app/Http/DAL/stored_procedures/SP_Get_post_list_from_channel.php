<?php
/**
 * Class SP_Get_post_list_from_channel
 *
 * This is an auto-generated class.
 * DO NOT EDIT IT ! It will be rewritten at next generation.
 */

namespace App\Http\DAL\stored_procedures;

class SP_Get_post_list_from_channel extends \Mrblackus\LaravelStoredprocedures\SPModel
{
  protected $post_id;

  public function getPost_id()
  {
    return $this->post_id;
  }

  public function setPost_id($post_id)
  {
    $this->post_id = $post_id;
  }
  protected $content;

  public function getContent()
  {
    return $this->content;
  }

  public function setContent($content)
  {
    $this->content = $content;
  }
  protected $created_at;

  public function getCreated_at()
  {
    return $this->created_at;
  }

  public function setCreated_at($created_at)
  {
    $this->created_at = $created_at;
  }
  protected $user_id;

  public function getUser_id()
  {
    return $this->user_id;
  }

  public function setUser_id($user_id)
  {
    $this->user_id = $user_id;
  }
  /**
   * @return SP_Get_post_list_from_channel[]
   */
  public static function execute($id)
  {
    $pdo   = \DB::connection()->getPdo();
    $query = $pdo->prepare('SELECT * FROM sp_get_post_list_from_channel(:id)');

    self::bindPDOValue($query, ':id', $id);

    $query->execute();
    $res = array();

    foreach ($query->fetchAll(\PDO::FETCH_ASSOC) as $v)
    {
      $object = new SP_Get_post_list_from_channel();
      self::hydrate($object, $v);
      $res[] = $object;
    }

    return $res;
  }
}