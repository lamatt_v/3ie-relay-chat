<?php
/**
 * Created by PhpStorm.
 * User: adrien.gandarias
 * Date: 08/04/2015
 * Time: 10:12
 */

namespace App\Http\DAL;


use App\Http\DAL\Models\Role;
use App\Http\DAL\Models\User;
use App\Http\DAL\stored_procedures\SP_Get_list_users;
use App\Http\DAL\stored_procedures\SP_Get_social_user;
use App\Http\DAL\stored_procedures\SP_Set_social_user;
use App\Http\DBO\UserLight;

class UserDAL
{
    #region converters
    /**
     * @param $user SP_Get_list_users
     * @return UserLight
     */
    private static function setUserLight($user)
    {
        $res = new UserLight();
        $res->setId($user->getId());
        $res->setUsername($user->getUsername());
        $res->setEmail($user->getEmail());

        return $res;
    }

    /**
     * @param $user SP_Get_social_user
     * @return \App\Http\DBO\User
     */
    private static function setUserFromSocial($user)
    {
        $res = new \App\Http\DBO\User();

        $res->setId($user->getId());
        $res->setUsername($user->getUsername());
        $res->setAvatarUrl($user->getAvatar_url());
        $res->setDeletedAt($user->getDeleted_at());
        $res->setEmail($user->getEmail());

        return $res;
    }
    #endregion

    /**
     * @param $user \App\Http\DBO\User
     * @return \App\Http\DBO\User
     */
    public static function create($user)
    {
        $user_to_create = new User();

        $user_to_create->username = $user->getUsername();
        $user_to_create->email = $user->getEmail();
        $user_to_create->password = $user->getPassword();
        $user_to_create->salt = $user->getSalt();
        $user_to_create->avatar_url = $user->getAvatarUrl();

        $user_to_create->save();
        $user->setId($user_to_create->id);

        return $user;
    }

    /**
     * @param $id
     * @return \App\Http\DBO\User
     */
    public static function find($id)
    {
        $user = User::find($id);
        /** @var $user User */

        if (!is_null($user))
            return $user->toBusinessModel();
        else
            return null;
    }

    /**
     * @param $email
     * @return \App\Http\DBO\AbstractModel|\App\Http\DBO\User|null
     */
    public static function findByEmail($email)
    {
        $user = User::where("email", "=", $email)->first();
        /** @var $user User */
        if (!is_null($user))
            return $user->toBusinessModel();
        else
            return null;
    }

    /**
     * @param $username
     * @return \App\Http\DBO\AbstractModel|\App\Http\DBO\User|null
     */
    public static function findByUsername($username)
    {
        $user = User::where("username", "=", $username)->first();
        /** @var $user User */
        if (!is_null($user))
            return $user->toBusinessModel();
        else
            return null;
    }

    /**
     * @param $user \App\Http\DBO\User
     * @return bool
     */
    public static function edit($user)
    {
        $user_to_update = User::find($user->getId());
        /** @var User $user_to_update */

        if (!is_null($user_to_update))
        {
            $user_to_update->email = $user->getEmail();
            $user_to_update->username = $user->getUsername();
            $user_to_update->password = $user->getPassword();
            $user_to_update->salt = $user->getSalt();
            $user_to_update->avatar_url = $user->getAvatarUrl();
            $user_to_update->avatar_url = $user->getAvatarUrl();
            $user_to_update->save();

            return true;
        }
        else
            return false;
    }

    /**
     * @param $social_id
     * @param $social_name
     * @return \App\Http\DBO\User
     */
    public static function getSocialUser($social_id, $social_name)
    {
        $res = SP_Get_social_user::execute($social_id, $social_name);
        /** @var $res SP_Get_social_user[] */
        if (count($res) > 0)
            return self::setUserFromSocial($res[0]);
        else
            return null;
    }

    public static function setSocialUser($social_id, $social_name, $user_id)
    {
        SP_Set_social_user::execute($user_id, $social_name, $social_id);
    }

    /**
     * @param $page int
     * @param $limit int
     * @return UserLight[]
     */
    public static function getList($page, $limit)
    {
        $users = SP_Get_list_users::execute($page, $limit);
        $res = [];
        foreach ($users as $user)
            $res[] = self::setUserLight($user);

        return $res;
    }

    /**
     * @param $id
     * @return bool
     */
    public static function isAdmin($id)
    {
        $user = User::with("roles")->get()->find($id);
        /** @var $user User */

        $roles = $user->getRelation("roles");
        /** @var $roles Role[] */
        $res = false;
        for ($i = 0; $i < count($roles) && !$res; $i++)
        {
            $res = $roles[$i]->name == 'admin';
        }

        return $res;
    }

    /**
     * @return int
     */
    public static function count()
    {
        $count = User::where("deleted_at", "=", null)->count();
        return $count;
    }

    public static function delete($id)
    {
        $user = User::find($id);
        /** @var $user User */

        if (!is_null($user))
        {
            $user->deleted_at = date('r');
            $user->save();

            return true;
        }
        else
            return false;
    }
}