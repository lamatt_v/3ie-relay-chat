<?php
/**
 * Created by PhpStorm.
 * User: valentin.lamatte
 * Date: 13/02/16
 * Time: 22:09
 */

namespace App\Http\DAL;

use App\Http\DAL\Models\Image;

class ImageDAL
{
    /**
     * @param $image \App\Http\DBO\Image
     * @return \App\Http\DBO\Image
     */
    public static function create($image)
    {
        $image_to_create = new Image();

        $image_to_create->url = $image->getUrl();

        $image_to_create->save();
        $image->setId($image_to_create->id);

        return $image;
    }
}