<?php

/**
 * Created by PhpStorm.
 * User: Deniz
 * Date: 12/02/2016
 * Time: 14:12
 */
namespace App\Http\DAL;
use App\Http\DAL\Models\Channel;
use App\Http\DAL\stored_procedures\SP_Get_list_channels;
use App\Http\DAL\stored_procedures\SP_Get_post_list_from_channel;

class ChannelDAL
{
    public static function getList()
    {
        $channels = SP_Get_list_channels::execute();
        $dbos = array();
        foreach($channels as $channel)
        {
            $dbos[] = array("channelName" => $channel->getName(),
                            "channelId" => $channel->getId());
        }
        return $dbos;
    }

    /**
     * @param $id
     * @return \App\Http\DBO\Channel
     */
    public static function find($id)
    {
        $channel = Channel::find($id);
        /** @var $channel Channel */
        if (!is_null($channel))
            return $channel->toBusinessModel();
    }

    public static function getPosts($id)
    {
        $post = SP_Get_post_list_from_channel::execute();
    }

    public static function findByName($name)
    {
        $channel = Channel::where("name", "=", $name)->first();
        /** @var $channel Channel */
        if (!is_null($channel))
            return $channel->toBusinessModel();
        else
            return null;
    }

    /**
     * @param $channel \App\Http\DBO\Channel
     * @return \App\Http\DBO\Channel
     */
    public static function create($channel)
    {
        $channel_to_create = new Channel();
        $channel_to_create->name = $channel->getName();
        $channel_to_create->password = $channel->getPassword();
        $channel_to_create->save();
        $channel->setId($channel_to_create->id);
        return $channel;
    }
}