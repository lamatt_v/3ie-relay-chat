<?php
/**
 * Created by JetBrains PhpStorm.
 * User: cedric.schneider
 * Date: 16/07/15
 * Time: 16:56
 * To change this template use File | Settings | File Templates.
 */
namespace App\Http\DAL\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Http\DAL\Models\Role
 *
 * @property integer $id
 * @property string $name
 */
class Role extends AbstractModel
{
    protected $table = "roles";

    public function users()
    {
        return $this->belongsToMany('\App\Http\DAL\Models\User', "users_roles");
    }

    public function toBusinessModel()
    {
        $role = new \App\Http\DBO\Role();
        $role->setId($this->id);
        $role->setName($this->name);

        return $role;
    }

}