<?php
/**
 * Created by PhpStorm.
 * User: Deniz
 * Date: 12/02/2016
 * Time: 14:35
 */

namespace App\Http\DAL\Models;
use App\Http\DBO\ChannelDBO;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Http\DAL\Models\Channel
 * @property int $id
 * @property string $name
 * @property string $password
 */
class Channel extends AbstractModel{
    public function toBusinessModel()
    {
        $channel = new \App\Http\DBO\Channel();
        $channel->setId($this->id);
        $channel->setName($this->name);
        $channel->setPassword($this->password);
        return $channel;

    }
    public function setUpdatedAtAttribute($value)
    {
        // to Disable updated_at
    }
    public function setCreatedAt($value)
    {
        // to disable created_at
    }
}