<?php
/**
 * Created by PhpStorm.
 * User: adrien.gandarias
 * Date: 08/04/2015
 * Time: 10:10
 */

namespace App\Http\DAL\Models;

use App\Http\DBO\UserDBO;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Http\DAL\Models\User
 *
 * @property integer $id 
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $salt
 * @property string $avatar_url
 * @property string $deleted_at
 */
class User extends AbstractModel
{
    protected $table = "users";

    public function roles()
    {
        return $this->belongsToMany('\App\Http\DAL\Models\Role', "users_roles");
    }

    public function users_social_networks()
    {
        return $this->hasMany('\App\Http\DAL\Models\User_social_network', 'social_network_id');
    }

    public function toBusinessModel()
    {
        $user = new \App\Http\DBO\User();

        $user->setId($this->id);
        $user->setUsername($this->username);
        $user->setAvatarUrl($this->avatar_url);
        $user->setPassword($this->password);
        $user->setSalt($this->salt);
        $user->setEmail($this->email);
        $user->setDeletedAt($this->deleted_at);

        return $user;
    }
}