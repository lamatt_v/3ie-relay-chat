<?php
/**
 * Created by PhpStorm.
 * User: valentin.lamatte
 * Date: 12/02/16
 * Time: 14:28
 */

namespace App\Http\DAL\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Http\DAL\Models\Post
 *
 * @property integer $id
 * @property string $content
 * @property integer $channel_id
 * @property string $created_at
 * @property integer $user_id
 */
class Post extends AbstractModel
{
    protected $table = "posts";

    /**
     * @return \App\Http\DBO\Post
     */
    public function toBusinessModel()
    {
        $post = new \App\Http\DBO\Post();

        $post->setCreatedAt($this->created_at);
        $post->setId($this->id);
        $post->setChannelId($this->channel_id);
        $post->setContent($this->content);
        $post->setUserId($this->user_id);

        return $post;
    }

    public function setUpdatedAtAttribute($value)
    {
        // to Disable updated_at
    }
}