<?php
/**
 * Created by JetBrains PhpStorm.
 * User: cedric.schneider
 * Date: 16/07/15
 * Time: 17:18
 * To change this template use File | Settings | File Templates.
 */
namespace App\Http\DAL\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Http\DAL\Models\Role
 *
 * @property integer $id
 * @property string $name
 */
class Social_network extends AbstractModel
{
    protected $table = "social_networks";

    public function users_social_networks()
    {
        return $this->hasMany('\App\Http\DAL\Models\User_social_network', 'user_id');
    }

    public function toBusinessModel()
    {
        $social_network = new \App\Http\DBO\Social_network();
        $social_network->setId($this->id);
        $social_network->setName($this->name);

        return $social_network;
    }
}
