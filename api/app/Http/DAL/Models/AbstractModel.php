<?php
/**
 * Created by JetBrains PhpStorm.
 * User: cedric.schneider
 * Date: 16/04/15
 * Time: 12:00
 * To change this template use File | Settings | File Templates.
 */

namespace App\Http\DAL\Models;

use Illuminate\Database\Eloquent\Model;

abstract class AbstractModel extends Model
{
    /**
     * Mapping from DBO model to business model.
     * If this model has no DBO equivalent, do nothing in this function
     * @param Model $eloquentModel
     * @return \App\Http\DBO\AbstractModel
     */
    public abstract function toBusinessModel();
}