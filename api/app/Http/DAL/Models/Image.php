<?php
/**
 * Created by PhpStorm.
 * User: valentin.lamatte
 * Date: 13/02/16
 * Time: 22:03
 */

namespace App\Http\DAL\Models;

use App\Http\DBO\UserDBO;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Http\DAL\Models\Image
 *
 * @property integer $id
 * @property string $url
 * @property string $created_at
 */
class Image extends AbstractModel
{
    protected $table = "images";

    public function posts()
    {
        return $this->belongsTo('\App\Http\DAL\Models\Posts', "post_media", "id");
    }

    public function toBusinessModel()
    {
        $image = new \App\Http\DBO\Image();

        $image->setId($this->id);
        $image->setUrl($this->url);
        $image->setCreatedAt($this->created_at);

        return $image;
    }

    public function setUpdatedAt($value)
    {
    }
}