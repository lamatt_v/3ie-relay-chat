<?php
/**
 * Created by JetBrains PhpStorm.
 * User: cedric.schneider
 * Date: 16/07/15
 * Time: 17:42
 * To change this template use File | Settings | File Templates.
 */
namespace App\Http\DAL\Models;

/**
 * App\Http\DAL\Models\User_social_network
 *
 * @property integer $user_id
 * @property integer $social_network_id
 * @property string $value
 */
class User_social_network extends AbstractModel
{
    protected $table = "users_social_networks";

    public function user()
    {
        return $this->belongsTo('\App\Http\DAL\Models\User', 'user_id');
    }

    public function social_network()
    {
        return $this->belongsTo('\App\Http\DAL\Models\Social_network', 'social_network_id');
    }

    public function toBusinessModel()
    {
        $user_social_network = new \App\Http\DBO\User_social_network();
        $user_social_network->setSocialNetworkId($this->social_network_id);
        $user_social_network->setUserId($this->user_id);
        $user_social_network->setValue($this->value);

        return $user_social_network;
    }
}