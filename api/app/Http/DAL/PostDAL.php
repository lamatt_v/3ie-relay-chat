<?php
/**
 * Created by PhpStorm.
 * User: valentin.lamatte
 * Date: 12/02/16
 * Time: 15:14
 */

namespace App\Http\DAL;


use App\Http\DAL\Models\Post;
use App\Http\DAL\stored_procedures\SP_Get_list_posts;

class PostDAL
{
    /**
     * @param $post \App\Http\DBO\Post
     * @return mixed
     */
    public static function create($post)
    {
        $post_to_create = new Post();

        $post_to_create->content = $post->getContent();
        $post_to_create->user_id = $post->getUserId();
        $post_to_create->channel_id = $post->getChannelId();
        $post_to_create->image_id = $post->getImageId();

        $post_to_create->save();
        $post->setId($post_to_create->id);

        return $post;
    }

    public static function getFromChannel($channel_id, $nb_res, $min_date)
    {
        $posts = SP_Get_list_posts::execute($min_date, $nb_res, $channel_id);
        $result = array();
        foreach($posts as $post) {
            //Ajouter au tableau
            $result[] = array(
                "user_id" => $post->getUser_id(),
                "content" => $post->getPost_content(),
                "date" => $post->getCreated_at(),
                "image" => $post->getImage_url());
        }
        return $result;
    }

}