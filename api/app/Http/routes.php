<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//users
Route::get('/user', 'UserController@getAll');
Route::post('/user/create', 'UserController@create');
Route::post('/user/login', 'UserController@login');
Route::get('/user/profile', ['middleware' => ['auth'], 'uses' => 'UserController@getProfile']);
Route::put('/user/change_password', ['middleware' => ['auth'], 'uses' => 'UserController@changePassword']);
Route::post('/user/facebook_login', 'UserController@facebookLogin');
Route::post('/user/twitter_login', 'UserController@twitterLogin');
Route::put('/user/edit_profile', ['middleware' => ['auth'], 'uses' => 'UserController@editProfile']);
Route::put('/user/edit_avatar', ['middleware' => ['auth'], 'uses' => 'UserController@editAvatar']);
Route::get('/user/list/{page}/{limit}', ['middleware' => ['auth_admin'], 'uses' => 'UserController@getList']);
Route::get('/user/get/{id}', ['middleware' => ['auth_admin'], 'uses' => 'UserController@get']);
Route::get('/user/auth_admin', ['middleware' => ['auth_admin'], 'uses' => 'UserController@authAdmin']);
Route::get('/user/count', ['middleware' => ['auth_admin'], 'uses' => 'UserController@count']);
Route::put('/user/edit', ['middleware' => ['auth_admin'], 'uses' => 'UserController@edit']);
Route::delete('/user/delete', ['middleware' => ['auth_admin'], 'uses' => 'UserController@delete']);

//channels

Route::get('/channel', 'ChannelController@getList');
Route::post('/channel/get/{id}', 'ChannelController@login')->where('channel_id', '[0-9]+');
Route::post('/channel/create', 'ChannelController@create');
//posts
Route::post('/post/{channel_id}', 'PostController@getFromChannel')->where('channel_id', '[0-9]+');
Route::post('/post/{channel_id}/create', ['middleware' => ['auth'], 'uses' => 'PostController@create'])->where('channel_id', '[0-9]+');
Route::post('/post/{channel_id}/post_img', ['middleware' => ['auth'], 'uses' => 'ImageController@postImage'])->where('channel_id', '[0-9]+');

