<?php
/**
 * Created by JetBrains PhpStorm.
 * User: cedric.schneider
 * Date: 29/04/15
 * Time: 11:31
 * To change this template use File | Settings | File Templates.
 */
namespace App\Http\Helpers;

use League\Flysystem\Exception;

class JWTException extends \Exception
{
}

class JWTHelper
{

    const MCRYPT_KEY =      "3d61cf256eeb3ed7e564fopm";

    const JWT_KEY =         "4d94a42390878c0ed3469c9371df9d78";
    const EXPIRATION_KEY =  "expiration_date";
    const USER_KEY =        "user_id";

    public static function decode($token)
    {
        $jwt = mcrypt_decrypt(MCRYPT_TRIPLEDES, self::MCRYPT_KEY, base64_decode($token), "ecb");
        try
        {
            $data = (array)\JWT::decode($jwt, self::JWT_KEY, ['HS256']);
        }
        catch (\Exception $e)
        {
            throw new JWTException("MALFORMED_JWT");
        }


        $expiration_date = array_key_exists(self::EXPIRATION_KEY, $data) ? $data[self::EXPIRATION_KEY] : null;
        if (!is_numeric($expiration_date))
            throw new JWTException("MALFORMED_JWT");
        $user_id = array_key_exists(self::USER_KEY, $data) ? $data[self::USER_KEY] : null;
        if (!is_numeric($user_id))
            throw new JWTException("MALFORMED_JWT");

        if ($expiration_date < time())
            throw new JWTException("EXPIRED_JWT");

        return $data;
    }

    public static function encode($data)
    {
        $date = new \DateTime("now", new \DateTimeZone("Europe/Paris"));
        $date->modify("+30 day");
        $date->setTime(0, 0, 0);
        $data[self::EXPIRATION_KEY] = $date->getTimestamp();

        return base64_encode(mcrypt_encrypt(MCRYPT_TRIPLEDES, self::MCRYPT_KEY, \JWT::encode($data, self::JWT_KEY), "ecb"));
    }
}