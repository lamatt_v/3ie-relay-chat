<?php

return array(
    'schema'                => 'public',
    'model_save_dir'        => 'Http/DAL/stored_procedures/',
    'model_namespace'       => 'App\\Http\\DAL\\stored_procedures'
);