﻿-- Function: sp_get_list_users(integer, integer)

-- DROP FUNCTION sp_get_list_users(integer, integer);

CREATE OR REPLACE FUNCTION sp_get_list_users(
    IN page integer,
    IN nb_res integer,
    OUT id integer,
    OUT username character varying,
    OUT email character varying)
  RETURNS SETOF record AS
$BODY$

SELECT u.id, u.username, u.email
FROM users u
WHERE u.deleted_at IS NULL
ORDER BY u.created_at
LIMIT $2 OFFSET ($1 - 1) * $2
$BODY$
  LANGUAGE sql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_get_list_users(integer, integer)
  OWNER TO postgres;
