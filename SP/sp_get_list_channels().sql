﻿-- Function: sp_get_list_channels()

-- DROP FUNCTION sp_get_list_channels();

CREATE OR REPLACE FUNCTION sp_get_list_channels(
    OUT id integer,
    OUT name character varying)
  RETURNS SETOF record AS
$BODY$

SELECT c.id, c.name
FROM channels c
ORDER BY c.id
$BODY$
  LANGUAGE sql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION sp_get_list_channels()
  OWNER TO postgres;
